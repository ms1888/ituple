/*
 * This file is part of ituple
 * Copyright (C) 2017-2023  Matija Skala <mskala@gmx.com>
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ITUPLE_HH
#define ITUPLE_HH

#include <stdexcept>
#include <type_traits>
#include "ituple/all_equal.hh"
#include "ituple/iterator.hh"

template <template <typename...> class Tp, typename... Types>
class ituple {
public:
    using iterator = ituplexx::iterator<Tp, Types...>;
    using const_iterator = ituplexx::const_iterator<Tp, Types...>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    ituple ( Tp<Types...>& tuple ) : tuple(tuple) {
        if ( std::apply([] (Types&... a) { return !ituplexx::all_equal(std::size(a)...); }, tuple) )
            throw std::invalid_argument{"ituple: non-matching sizes"};
    }
    std::size_t size() const { return std::size(std::get<0>(tuple)); }

private:
    template <std::nullptr_t>
    auto begin() { return std::apply([] (auto&&... a) { return iterator{ std::begin(a)... }; }, tuple); }
    template <std::nullptr_t>
    auto end() { return std::apply([] (auto&&... a) { return iterator{ std::end(a)... }; }, tuple); }
    template <std::nullptr_t>
    auto rbegin() { return std::apply([] (auto&&... a) { return reverse_iterator{ std::rbegin(a)... }; }, tuple); }
    template <std::nullptr_t>
    auto rend() { return std::apply([] (auto&&... a) { return reverse_iterator{ std::rend(a)... }; }, tuple); }

    template <std::nullptr_t>
    auto cbegin() const { return std::apply([] (auto&&... a) { return const_iterator{ std::begin(a)... }; }, tuple); }
    template <std::nullptr_t>
    auto cend() const { return std::apply([] (auto&&... a) { return const_iterator{ std::end(a)... }; }, tuple); }
    template <std::nullptr_t>
    auto crbegin() const { return std::apply([] (auto&&... a) { return const_reverse_iterator{ std::rbegin(a)... }; }, tuple); }
    template <std::nullptr_t>
    auto crend() const { return std::apply([] (auto&&... a) { return const_reverse_iterator{ std::rend(a)... }; }, tuple); }

public:
    iterator begin() { return begin<nullptr>(); }
    iterator end() { return end<nullptr>(); }
    reverse_iterator rbegin() { return rbegin<nullptr>(); }
    reverse_iterator rend() { return rend<nullptr>(); }

    const_iterator begin() const {
        return cbegin<nullptr>();
    }
    const_iterator end() const {
        return cend<nullptr>();
    }
    const_reverse_iterator rbegin() const {
        return crbegin<nullptr>();
    }
    const_reverse_iterator rend() const {
        return crend<nullptr>();
    }

    const_iterator cbegin() const { return begin(); }
    const_iterator cend() const { return end(); }
    const_reverse_iterator crbegin() const { return rbegin(); }
    const_reverse_iterator crend() const { return rend(); }

private:
    Tp<Types...>& tuple;
};

template <template <typename...> class Tp, typename... Types>
class ituple_rval {
public:
    using iterator = ituplexx::iterator<Tp, Types...>;
    using const_iterator = ituplexx::const_iterator<Tp, Types...>;
    using reverse_iterator = std::reverse_iterator<iterator>;
    using const_reverse_iterator = std::reverse_iterator<const_iterator>;

    ituple_rval ( Tp<Types...>&& tuple ) : tuple(std::move(tuple)) {
        if ( std::apply([] (Types&... a) { return !ituplexx::all_equal(std::size(a)...); }, this->tuple) )
            throw std::invalid_argument{"ituple: non-matching sizes"};
    }
    std::size_t size() const { return std::size(std::get<0>(tuple)); }

    template <std::nullptr_t>
    auto begin() { return std::apply([] (auto&&... a) { return iterator{ std::begin(a)... }; }, tuple); }
    template <std::nullptr_t>
    auto end() { return std::apply([] (auto&&... a) { return iterator{ std::end(a)... }; }, tuple); }
    template <std::nullptr_t>
    auto rbegin() { return std::apply([] (auto&&... a) { return reverse_iterator{ std::rbegin(a)... }; }, tuple); }
    template <std::nullptr_t>
    auto rend() { return std::apply([] (auto&&... a) { return reverse_iterator{ std::rend(a)... }; }, tuple); }

    template <std::nullptr_t>
    auto cbegin() const { return std::apply([] (auto&&... a) { return const_iterator{ std::begin(a)... }; }, tuple); }
    template <std::nullptr_t>
    auto cend() const { return std::apply([] (auto&&... a) { return const_iterator{ std::end(a)... }; }, tuple); }
    template <std::nullptr_t>
    auto crbegin() const { return std::apply([] (auto&&... a) { return const_reverse_iterator{ std::rbegin(a)... }; }, tuple); }
    template <std::nullptr_t>
    auto crend() const { return std::apply([] (auto&&... a) { return const_reverse_iterator{ std::rend(a)... }; }, tuple); }

    iterator begin() { return begin<nullptr>(); }
    iterator end() { return end<nullptr>(); }
    reverse_iterator rbegin() { return rbegin<nullptr>(); }
    reverse_iterator rend() { return rend<nullptr>(); }

    const_iterator begin() const {
        return cbegin<nullptr>();
    }
    const_iterator end() const {
        return cend<nullptr>();
    }
    const_reverse_iterator rbegin() const {
        return crbegin<nullptr>();
    }
    const_reverse_iterator rend() const {
        return crend<nullptr>();
    }

    const_iterator cbegin() const { return begin(); }
    const_iterator cend() const { return end(); }
    const_reverse_iterator crbegin() const { return rbegin(); }
    const_reverse_iterator crend() const { return rend(); }

private:
    Tp<Types...> tuple;
};

template <template <typename...> class Tp, typename... Types>
ituple<Tp, Types...> to_ituple ( Tp<Types...>& tuple ) { return tuple; }

template <template <typename...> class Tp, typename... Types>
std::enable_if_t<std::is_rvalue_reference_v<Tp<Types...>&&>, ituple_rval<Tp, Types...>> to_ituple ( Tp<Types...>&& tuple ) { return std::move(tuple); }

#endif // ITUPLE_HH
