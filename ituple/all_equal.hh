/*
 * This file is part of ituple
 * Copyright (C) 2017-2023  Matija Skala <mskala@gmx.com>
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ITUPLE_ALL_EQUAL_HH
#define ITUPLE_ALL_EQUAL_HH

#include <utility>

namespace ituplexx {

template <typename...>
struct AllEqual;

template <typename FirstArg, typename... OtherArgs>
struct AllEqual<FirstArg,OtherArgs...> {
    bool operator() ( FirstArg&& first, OtherArgs&&... args ) {
        for ( bool ne: { (first != args)... } )
            if ( ne ) return false;
        return true;
    }
};

template <typename OnlyOneArg>
struct AllEqual<OnlyOneArg> {
    bool operator() ( OnlyOneArg&& ) { return true; }
};

template <>
struct AllEqual<> {
    bool operator() () { return true; }
};

template <typename... Args>
bool all_equal ( Args&&... args ) { return AllEqual<Args&&...>{} ( std::forward<Args>(args)... ); }
}

#endif // ITUPLE_ALL_EQUAL_HH
