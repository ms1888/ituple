/*
 * This file is part of ituple
 * Copyright (C) 2017-2023  Matija Skala <mskala@gmx.com>
 *
 * This library is free software: you can redistribute it and/or
 * modify it under the terms of the GNU Lesser General Public
 * License as published by the Free Software Foundation, either
 * version 3 of the License, or (at your option) any later version.
 *
 * This library is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
 * Lesser General Public License for more details.
 *
 * You should have received a copy of the GNU Lesser General Public
 * License along with this library.  If not, see <http://www.gnu.org/licenses/>.
 *
 */

#ifndef ITUPLE_ITERATOR_HH
#define ITUPLE_ITERATOR_HH

#include <tuple>

namespace ituplexx {

template<template<typename...> class T, typename... Types>
struct IteratorHelper;

template<template<typename...> class T>
struct IteratorHelper<T> {
    template<typename... Saved>
    using iterator_t = T<Saved...>;
    template<typename... Saved>
    using const_iterator_t = T<Saved...>;
};

template<template<typename...> class T, typename Type>
struct IteratorHelper<T, Type> {
    template<typename... Saved>
    using iterator_t = T<Saved..., std::conditional_t<std::is_const_v<Type>, typename Type::const_iterator, typename Type::iterator>>;
    template<typename... Saved>
    using const_iterator_t = T<Saved..., typename Type::const_iterator>;
};

template<template<typename...> class T, typename FirstType, typename... OtherTypes>
struct IteratorHelper<T, FirstType, OtherTypes...> {
    template<typename... Saved>
    using iterator_t = typename IteratorHelper<T, OtherTypes...>::template iterator_t<Saved..., std::conditional_t<std::is_const_v<FirstType>, typename FirstType::const_iterator, typename FirstType::iterator>>;
    template<typename... Saved>
    using const_iterator_t = typename IteratorHelper<T, OtherTypes...>::template const_iterator_t<Saved..., typename FirstType::const_iterator>;
};

template<typename base, template<typename...> class T>
struct Iterator : base {
    using base::base;
    auto operator*() {
        return std::apply([] (auto&&... a) { return T<decltype(*a)...>{ *a... }; }, static_cast<base&>(*this));
    }
    Iterator& operator++() {
        std::apply([] (auto&... a) { (++a, ...); }, static_cast<base&>(*this));
        return *this;
    }
    Iterator operator++(int) {
        auto ret = *this;
        std::apply([] (auto&... a) { (a++, ...); }, static_cast<base&>(*this));
        return ret;
    }
    Iterator& operator--() {
        std::apply([] (auto&... a) { (--a, ...); }, static_cast<base&>(*this));
        return *this;
    }
    Iterator operator--(int) {
        auto ret = *this;
        std::apply([] (auto&... a) { (a--, ...); }, static_cast<base&>(*this));
        return ret;
    }
};

template<template<typename...> class T, typename... Types>
using iterator = Iterator<typename IteratorHelper<T, Types...>::template iterator_t<>, T>;

template<template<typename...> class T, typename... Types>
using const_iterator = Iterator<typename IteratorHelper<T, Types...>::template const_iterator_t<>, T>;
}

#endif // ITUPLE_ITERATOR_HH
